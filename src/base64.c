#include "base64.h"

#include <assert.h>
#include <ctype.h>
#include <errno.h>

#define BASE64_PAD '='

/* BASE 64 encode table */
static const char encode_map[] = {
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
    'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
    'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/',
};

static const uint8_t decode_map[] = {
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x3e, 0xff, 0xff, 0xff, 0x3f,
    0x34, 0x35, 0x36, 0x37, 0x38, 0x39, 0x3a, 0x3b, 0x3c, 0x3d, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0x0,  0x1,  0x2,  0x3,  0x4,  0x5,  0x6,
    0x7,  0x8,  0x9,  0xa,  0xb,  0xc,  0xd,  0xe,  0xf,  0x10, 0x11, 0x12,
    0x13, 0x14, 0x15, 0x16, 0x17, 0x18, 0x19, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f, 0x20, 0x21, 0x22, 0x23, 0x24,
    0x25, 0x26, 0x27, 0x28, 0x29, 0x2a, 0x2b, 0x2c, 0x2d, 0x2e, 0x2f, 0x30,
    0x31, 0x32, 0x33, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
    0xff, 0xff, 0xff, 0xff,
};
static const uint8_t DECODE_INVALID_INDEX = 0xff;

// shamelessly copy/pasted from
// https://cs.opensource.google/go/go/+/master:src/encoding/base64/base64.go
char *base64_encode(const uint8_t *src, size_t src_len, size_t *dst_len) {
    assert(src);
    assert(dst_len);

    *dst_len  = BASE64_ENCODE_LEN(src_len);
    char *dst = malloc(*dst_len);
    if (dst == NULL) {
        return NULL;
    }

    if (src_len == 0) {
        return dst;
    }

    size_t si = 0, di = 0;  // src, dest index
    uint32_t val;
    size_t n = (src_len / 3) * 3;
    while (si < n) {
        // Convert 3x 8bit source bytes into 4 bytes
        val = (uint32_t)(src[si + 0]) << 16 | (uint32_t)(src[si + 1]) << 8 |
              (uint32_t)(src[si + 2]);

        dst[di + 0] = encode_map[val >> 18 & 0x3f];
        dst[di + 1] = encode_map[val >> 12 & 0x3f];
        dst[di + 2] = encode_map[val >> 6 & 0x3f];
        dst[di + 3] = encode_map[val & 0x3f];

        si += 3;
        di += 4;
    }

    size_t remain = src_len - si;
    if (remain == 0) {
        return dst;
    }
    // Add the remaining small block
    val = (uint32_t)(src[si + 0]) << 16;
    if (remain == 2) {
        val |= (uint32_t)(src[si + 1]) << 8;
    }

    dst[di + 0] = encode_map[val >> 18 & 0x3f];
    dst[di + 1] = encode_map[val >> 12 & 0x3f];

    switch (remain) {
        case 2:
            dst[di + 2] = encode_map[val >> 6 & 0x3f];
            dst[di + 3] = BASE64_PAD;
            break;

        case 1:

            dst[di + 2] = BASE64_PAD;
            dst[di + 3] = BASE64_PAD;
            break;
    }

    return dst;
}

uint8_t *base64_decode(const char *src, size_t src_len, size_t *dst_len) {
    while (isspace(src[src_len - 1])) {
        src_len--;
    }
    if ((src_len % 4) != 0) {
        errno = EINVAL;
        return NULL;
    }

    size_t paddings = 0;
    if (src[src_len - 1] == BASE64_PAD) {
        paddings++;
    }
    if (src[src_len - 2] == BASE64_PAD) {
        paddings++;
    }

    *dst_len     = BASE64_DECODE_LEN(src_len, paddings);
    uint8_t *dst = malloc(*dst_len);
    if (dst == NULL) {
        return NULL;
    }

    if (*dst_len == 0) {
        return dst;
    }

    uint32_t tmp;
    size_t si = 0, di = 0;
    uint8_t unmapped;
    while (si < (src_len - 4)) {
        tmp = 0;
        for (int i = 3; i >= 0; i--) {
            unmapped = decode_map[src[si++]];
            if (unmapped == DECODE_INVALID_INDEX) {
                free(dst);
                errno = EINVAL;
                return NULL;
            }
            tmp |= ((uint32_t)unmapped) << (i * 6);
        }

        dst[di++] = (tmp >> 16) & 0xff;
        dst[di++] = (tmp >> 8) & 0xff;
        dst[di++] = tmp & 0xff;
    }

    tmp = 0;
    for (int i = 3; i >= (int)paddings; i--) {
        unmapped = decode_map[src[si++]];
        if (unmapped == DECODE_INVALID_INDEX) {
            free(dst);
            errno = EINVAL;
            return NULL;
        }
        tmp |= ((uint32_t)unmapped) << (i * 6);
    }

    switch (paddings) {
        case 0:
            dst[di++] = (tmp >> 16) & 0xff;
            dst[di++] = (tmp >> 8) & 0xff;
            dst[di++] = tmp & 0xff;
            break;
        case 1:  // rest four characters will decode to two bytes
            dst[di++] = (tmp >> 16) & 0xff;
            dst[di++] = (tmp >> 8) & 0xff;
            break;
        case 2:  // rest four characters wil deconde to single byte
            dst[di++] = (tmp >> 16) & 0xff;
            break;
    }

    return dst;
}
