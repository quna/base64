#include "base64.h"

#include <assert.h>
#include <string.h>

static void decode_test(const char *src, size_t src_len,
                        const char *expected_out, size_t expected_len) {
    size_t decoded_len;
    uint8_t *dest = base64_decode(src, src_len, &decoded_len);
    assert(dest);
    assert(decoded_len == expected_len);
    assert(strncmp(expected_out, (char *)dest, expected_len) == 0);
    free(dest);
}

int main() {
    // decode_test("", 0, "", 0);
    decode_test("Zg==", 4, "f", 1);
    decode_test("Zm8=", 4, "fo", 2);
    decode_test("Zm9v", 4, "foo", 3);
    decode_test("Zm9vYg==", 8, "foob", 4);
    decode_test("Zm9vYmE=", 8, "fooba", 5);
    decode_test("Zm9vYmFy", 8, "foobar", 6);

    // // Wikipedia examples
    decode_test("bGVhc3VyZS4=", 12, "leasure.", 8);
    decode_test("ZWFzdXJlLg==", 12, "easure.", 7);
    decode_test("YXN1cmUu", 8, "asure.", 6);
    decode_test("c3VyZS4=", 8, "sure.", 5);
    decode_test("c3VyZQ==", 8, "sure", 4);
    decode_test("c3Vy", 4, "sur", 3);
    decode_test("c3U=", 4, "su", 2);

    // // RFC 3548 examples
    decode_test("FPucA9l+", 8, "\x14\xfb\x9c\x03\xd9\x7e", 6);
    decode_test("FPucA9k=", 8, "\x14\xfb\x9c\x03\xd9", 5);
    decode_test("FPucAw==", 8, "\x14\xfb\x9c\x03", 4);

    decode_test("YmFzZQ==", 8, "base", 4);
    decode_test("YmFzZTY0", 8, "base64", 6);
    decode_test("TG9yZW0gSXBzdW0=", 16, "Lorem Ipsum", 11);

    return 0;
}