#include "base64.h"

#include <assert.h>
#include <string.h>

static void encode_test(const uint8_t *src, size_t src_len,
                        const char *expected_out, size_t expected_len) {
    size_t encoded_len;
    char *dest = base64_encode(src, src_len, &encoded_len);
    assert(dest);
    assert(encoded_len == expected_len);
    assert(strncmp(expected_out, dest, expected_len) == 0);
    free(dest);
}

int main() {
    // RFC 4648 examples
    encode_test((uint8_t *)"", 0, "", 0);
    encode_test((uint8_t *)"f", 1, "Zg==", 4);
    encode_test((uint8_t *)"fo", 2, "Zm8=", 4);
    encode_test((uint8_t *)"foo", 3, "Zm9v", 4);
    encode_test((uint8_t *)"foob", 4, "Zm9vYg==", 8);
    encode_test((uint8_t *)"fooba", 5, "Zm9vYmE=", 8);
    encode_test((uint8_t *)"foobar", 6, "Zm9vYmFy", 8);

    // Wikipedia examples
    encode_test((uint8_t *)"leasure.", 8, "bGVhc3VyZS4=", 12);
    encode_test((uint8_t *)"easure.", 7, "ZWFzdXJlLg==", 12);
    encode_test((uint8_t *)"asure.", 6, "YXN1cmUu", 8);
    encode_test((uint8_t *)"sure.", 5, "c3VyZS4=", 8);
    encode_test((uint8_t *)"sure", 4, "c3VyZQ==", 8);
    encode_test((uint8_t *)"sur", 3, "c3Vy", 4);
    encode_test((uint8_t *)"su", 2, "c3U=", 4);

    // RFC 3548 examples
    encode_test((uint8_t *)"\x14\xfb\x9c\x03\xd9\x7e", 6, "FPucA9l+", 8);
    encode_test((uint8_t *)"\x14\xfb\x9c\x03\xd9", 5, "FPucA9k=", 8);
    encode_test((uint8_t *)"\x14\xfb\x9c\x03", 4, "FPucAw==", 8);

    encode_test((uint8_t *)"base", 4, "YmFzZQ==", 8);
    encode_test((uint8_t *)"base64", 6, "YmFzZTY0", 8);
    encode_test((uint8_t *)"Lorem Ipsum", 11, "TG9yZW0gSXBzdW0=", 16);

    return 0;
}