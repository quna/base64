#include "base64.h"

#include <assert.h>
#include <stdbool.h>
#include <string.h>

// taken from
// https://cs.opensource.google/go/go/+/master:src/encoding/base64/base64_test.go

#define ARRAY_LENGTH(x) (sizeof(x) / sizeof((x)[0]))

typedef struct {
    const char *input;
    bool valid;
} test_case_t;

static const test_case_t test_cases[] = {
    {"", true},          {"\n", true},
    {"AA==", true},      {"AAA=", true},
    {"AAA=\n", true},    {"AAAA", true},
    {"AAAA\n", true},    {"!!!!", false},
    {"====", false},     {"x===", false},
    {"=AAA", false},     {"A=AA", false},
    {"AA=A", false},     {"AA==A", false},
    {"AAA=AAAA", false}, {"AAAAA", false},
    {"AAAAAA", false},   {"A=", false},
    {"A==", false},      {"AA=", false},
    {"AAAAAA=", false},  {"YWJjZA=====", false},
    {"A!\n", false},     {"A=\n", false},
};

int main() {
    for (size_t i = 0; i < ARRAY_LENGTH(test_cases); i++) {
        size_t dest_len;
        uint8_t *dest = base64_decode(test_cases[i].input,
                                      strlen(test_cases[i].input), &dest_len);
        if (test_cases[i].valid) {
            assert(dest != NULL);
            free(dest);
        } else {
            assert(dest == NULL);
        }
    }

    return 0;
}
