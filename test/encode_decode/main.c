#include "base64.h"

#include <assert.h>
#include <string.h>

const char lorem_ipsum[] =
    "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin "
    "imperdiet orci orci, non rhoncus nisl dignissim at. Mauris nec nunc "
    "sagittis, pharetra mi eget, sodales diam. Aenean neque dolor, "
    "suscipit eget ornare et, facilisis id justo. Integer tristique, ex "
    "sed euismod dignissim, lorem ex malesuada nisl, sed venenatis tortor "
    "dui in ipsum. In et consequat lacus. Aliquam ornare nisl vitae massa "
    "porta, a sagittis ipsum mattis. Donec vel sagittis risus. Vestibulum "
    "volutpat ultricies purus vel varius. In ac pretium nibh. Phasellus "
    "porttitor enim ac hendrerit luctus. Interdum et malesuada fames ac "
    "ante ipsum primis in faucibus. Nullam eleifend felis quis gravida "
    "porta.";

int main() {
    size_t src_len = strlen(lorem_ipsum);

    size_t encoded_len;
    char *encoded_str =
        base64_encode((const uint8_t *)lorem_ipsum, src_len, &encoded_len);
    assert(encoded_str);

    size_t decoded_len;
    uint8_t *decoded_str =
        base64_decode(encoded_str, encoded_len, &decoded_len);
    assert(decoded_str);

    assert(decoded_len == src_len);
    assert(strncmp(lorem_ipsum, decoded_str, src_len) == 0);

    free(encoded_str);
    free(decoded_str);

    return 0;
}