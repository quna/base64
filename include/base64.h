#pragma once

#include <stdint.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/**
 * \brief calculates length of encoded data from source length
 *
 */
#define BASE64_ENCODE_LEN(n) (((4 * (n) / 3) + 3) & ~3)
/**
 * \brief calculates length of decoded data from encoded length & padding count
 *
 */
#define BASE64_DECODE_LEN(len, padding) (((3 * (len)) / 4) - (padding))

/**
 * \brief encodes data to base64 encoded characters
 * The data is encoded with padding
 *
 * \param src data to encode
 * \param src_len length of src
 * \param dest_len pointer to store length of encoded data
 * \return base64 encoded data
 * \note return value is not '\0' terminated
 */
char *base64_encode(const uint8_t *src, size_t src_len, size_t *dest_len);

/**
 * \brief decodes base64 encoded data (with padding)
 *
 * \param src encoded data
 * \param src_len length of encoded data
 * \param dest_len pointer to store length of decoded data
 * \return decoded data
 */
uint8_t *base64_decode(const char *src, size_t src_len, size_t *dest_len);

#ifdef __cplusplus
}
#endif
