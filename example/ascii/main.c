#include "base64.h"

#include <assert.h>
#include <stdio.h>
#include <string.h>

int main() {
    char ascii[]     = "ascii";
    size_t ascii_len = strlen(ascii);

    size_t encode_len;
    char *encoded = base64_encode((uint8_t *)ascii, strlen(ascii), &encode_len);
    assert(encoded);
    printf("base64_encode('%s')='%.*s'\n", ascii, (int)encode_len, encoded);

    size_t decode_len;
    char *decoded = base64_decode(encoded, encode_len, &decode_len);
    assert(decoded);
    printf("base64_decode('%.*s')='%.*s'\n", (int)encode_len, encoded,
           (int)decode_len, decoded);

    free(encoded);
    free(decoded);

    return 0;
}