# Base64

Implements base64 encoding as specified by [RFC 4648](https://www.rfc-editor.org/rfc/rfc4648).

## References
- [Go base64 implementation](https://cs.opensource.google/go/go/+/master:src/encoding/base64)

